#!/bin/bash

## declaring array list for profile to create users
fname=('buddhi' 'jon' 'sanjay' 'prakash' 'isor')
lname=('tuladhar' 'cena' 'hoona' 'chitrakar' 'goorung')
eemail=('abc.com' 'jon@abc.com' 'sanjay@abc.com' 'prakash@abc.com' 'isor@abc.com')
username=('abc.com' 'jon@abc.com' 'sanjay@abc.com' 'prakash@abc.com' 'isor@abc.com')
cellphoneno=('112211' '8589' '36251' '356552' '25656564')

yourOktaDomain=exampleabcokta.com


## create 5 users using api provided by okta
## first set api_token as env var EG: export api_token=testapitoken123
## using a simple for loop to call okta api to create 5 users in example domain
for i in $(seq 0 4)
do
curl -v -X POST \
-H "Accept: application/json" \
-H "Content-Type: application/json" \
-H "Authorization: SSWS ${api_token}" \
-d '{
  "profile": {
    "firstName": "${fname[$i]}",
    "lastName": "${lname[$i]}",
    "email": "{$eemail[$i]}",
    "login": "{$username[$i]}",
    "mobilePhone": "{$cellphoneno[$i]}"
  }
}' "https://${yourOktaDomain}/api/v1/users?activate=false"

done

## lising all the user
curl -v -X GET \
	-H "Accept: application/json" \
	-H "Content-Type: application/json" \
	-H "Authorization: SSWS ${api_token}" \
	"https://${yourOktaDomain}/api/v1/users?limit=200"


## delete user according user id 
## here user id is 
userId=exampleuserid

curl -v -X DELETE \
	-H "Accept: application/json" \
	-H "Content-Type: application/json" \
	-H "Authorization: SSWS ${api_token}" \
	"https://${yourOktaDomain}/api/v1/users/${userId}?sendEmail=true"


