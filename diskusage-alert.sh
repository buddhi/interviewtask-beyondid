#!/bin/bash

## at first we need pem file or our rsa_pub public ssh key accepted it target server where we need to see disk uses
## -x option excludes the virtual partation. in our case tmpfs and squashfs as required for us
## then i  use simple array and for loop inside very simple if else conditon to check the disk uses.
## we can use number of HOSTIP and SSHUSER as our need but i have access to these 2 servers for checking this script so i used 2 host. We can make it 5 HOSTS also as requied by your question.

SSHUSER=('ubuntu' 'ubuntu')
HOSTIP=('54.241.17.160' '54.241.17.161') 

## eg this partation is less then 90 percent so i used it to check first condition less then 90%
for i in $(seq 0 1)
	do
		USEP=`ssh -i ogb.pem ${SSHUSER[$i]}@${HOSTIP[$i]} 'df -h -x tmpfs -x squashfs' | grep /dev/xvda1 | awk '{ printf "%d", $5}' ` 
		
		if [[ $USEP -gt 90 ]]
		then
			  echo "Your disk space is getting low"
		else
			  echo "Your disk is $USEP% Used"
		fi
	done

## eg this partation is 100% full so i used it to check else conditon greater then 90%
for i in $(seq 0 1)
	do
		USEP=`ssh -i ogb.pem ${SSHUSER[$i]}@${HOSTIP[$i]} 'df -h -x tmpfs -x squashfs' | grep /dev/mapper/ghampower-backups | awk '{ printf "%d", $5}' ` 
		
		if [[ $USEP -gt 90 ]]
		then
			  echo "Your disk space is getting low $USEP% used"
		else
			  echo "Your disk is $USEP% Used"
		fi
	done
